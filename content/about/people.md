---
title: "People"
date: 2024-10-08T22:27:05+03:00
---

Several individuals have contributed to **SIDLOC** over time.
Below is a list of the people currently (Oct 08, 2024) involved in the project, along with their respective roles in it.

| Role | Name |
| --- | --- |
| Documentation engineer | Vasilis Tsiligiannis |
| Electronics design reviewer | Agis Zisimatos |
| Electronics design reviewer | Daniel Bita |
| Kanban board coordinator | Vasilis Tsiligiannis |
| Mechanical designer | Manthos Papamatthaiou |
| Mechanical designer | Thanos Patsas |
| Project champion | Manthos Papamatthaiou |
| Project manager | Vasilis Tsiligiannis |
| Static website maintainer | Vasilis Tsiligiannis |
