---
title: "Context"
date: 2021-06-30T14:11:22+03:00
---

## Landscape ##

The space landscape is changing rapidly.
Especially since the density of objects in space has been increasing exponentially and so has the number of operators, space activities and architectures, which vary considerably.
For instance, there are constellations of satellites, comprised of hundreds of small satellites, PocketQubes and CubeSats.
The latter can often complicate things as they can be “part of a rapid deployment of scores of similar CubeSats into nearly identical orbits, which limits the ability of existing SSA/STM systems to react to the increased population of new space objects''.
All these issues constitute orbital prediction a daunting task; as the proliferation of missions, explorations, different types of satellites increases, so do the concerns about awareness in this domain.

Due to the expected increase of traffic in space, Space Satellite Awareness becomes imperative; according to some estimations, the number of active satellites will increase manifold in the coming years stating that "Between 2018 and 2026, at least another 3,000 satellites are expected to be launched".
This reality constitutes Space Situational Awareness, a vital parameter contributing to the success of satellite operations.
Moreover, as the world dependence on space increases, it becomes evident that disruptions to satellites may cause serious vulnerabilities and have an impact on a wide range of fields and disciplines; including security, infrastructure and taking a tremendous toll on the economy, science and society in general.
This impact places the attention on the need to track and identify the satellites successfully, while also monitoring and predicting the position of all space objects, space debris included.
"Maintaining accurate and precise awareness of the locations of satellites and debris and the environment around them, in addition to ensuring operation free from radio frequency interference, is increasingly being seen as critical for safe and sustainable operations in space." (ibid)


## Technical Objectives ##

Given the drastically different arena being shaped in space and the novel requirements such as a need for tightly packed constellations, orbital prediction and collision avoidance algorithms, and the need to provide next generation identification services, a properly designed identification protocol including all aspects (concept of operation, physical layer, architecture, software, ground segment e.t.c.) needs to be implemented.
This is in a nutshell, the target of this ITT.
Although several identification methods currently exist and provide useful data (ground RF radar tracking is a good example) a series of challenges are emerging for next generation identification systems.

- Fast identification 
- Increased system capacity (thousands of satellites)
- System reliability (positive ID, ID regardless of satellite status)
- Low power requirements and minimal size to support CubeSat and/or PicoSat systems
- Early pre-orbit functionality (immediate post-deployment activation)
- Post-mission disposal requirements

It becomes apparent from the above checklist that a properly designed, simple, commonly acceptable system needs to be implemented.
To address that we propose a series of actions that include:

- Space Industry consultation
- Open source / Open hardware ID architecture
- Existing ground segment (SatNOGS) that lowers both implementation as well as deployment costs and increases security and uptime
- High capacity link using contemporary un-attended spectrum access models and result in very low link power budget
- Expandability in the future to more elaborate and service rich software layers without sacrificing backwards compatibility

The target is to provide a system easy to integrate even in ¼ U or PQ satellites with minimal integration requirements so as to increase industry adoption.
Το achieve that a series of design choices need to be made within this project; Is a dedicated frequency band necessary?
Will an autonomous power system be necessary to ensure ID functionality beyond Satellite lifetime?
Which is the data relayed and in what format?
Such design choices are to be answered within this activity, experimented and publicly submitted to the community.
